# -----------------------------------------------------------------------------
# Generate help output when running just `make`
# -----------------------------------------------------------------------------
.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

help:
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

# -----------------------------------------------------------------------------
# Variables
# -----------------------------------------------------------------------------

pi_user=ouc
pi_ip=192.168.1.200

# -----------------------------------------------------------------------------
# SSH / Rsync
# -----------------------------------------------------------------------------

ssh_copy_id:  ## Copy ssh key to pi
	ssh-copy-id -i ~/.ssh/id_rsa.pub ${user_at_ip}

rsync_to_pi:  # Rsync to pi
	rsync -avzP bash_scripts/ ${user_at_ip}:~/bash-scripts --exclude ".DS_Store"

# -----------------------------------------------------------------------------
# Mutagen (if no manually rsyncing )
# -----------------------------------------------------------------------------

user_at_ip=${pi_user}@${pi_ip}
mutagen_sync_name=pi-starter-kit
mutagen_beta_dir=~/pi-starter-kit

mutagen_up:  ## Mutagen create
	mutagen sync create --name=${mutagen_sync_name} . ${user_at_ip}:${mutagen_beta_dir}

mutagen_pause:  ## Mutagen pause
	mutagen sync pause ${mutagen_sync_name}

mutagen_resume:  ## Mutagen resume
	mutagen sync resume ${mutagen_sync_name}

mutagen_down:  ## Mutagen terminate
	mutagen sync terminate ${mutagen_sync_name}

# -----------------------------------------------------------------------------
