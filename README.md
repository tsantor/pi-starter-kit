# Raspberry Pi Starter Kit

## Overview
A collection of useful scripts to do a handful of common tasks when setting up a new Pi. These scripts have been tested on "Raspbian GNU/Linux 11 (bullseye)".

## Scripts
- Bootstrap
- Install Docker
- Install Phidget Libraries
- Install Pillow
- Install Pyenv
- Install Python 3.x
- Install Teamviewer

- Other Misc. Scripts

## Usage
```
rsync -avzrp bash-scripts pi@192.168.1.x
```

Or use the `Makefile` (be sure to edit the variables at the top). Run `make` to view a list of available commands.

# Getting Started

## Flash the OS onto the Micro SD card

1. Download [Balena Etcher](https://www.balena.io/etcher/)
1. Download the latest [Raspbian OS](https://www.raspberrypi.org/downloads/raspbian/) - download the "Raspbian Stretch with desktop" zip
1. Extract the downloaded .zip file which will result in an .img file
1. Use Balena Etcher to flash the .img onto the Micro SD card

> NOTE: Even easier is download and install [Raspberry Pi Imager](https://www.raspberrypi.org/software/). Flash Raspberry Pi OS to the SD card. Follow the simple instructions.


## Monitor, Mouse & Keyboard
For at least the first run of a Raspberry Pi you will need a monitor, mouse and keyboard so you can see what you are doing.

## Raspbian Setup
When first booting the Raspberry Pi you will be presented with a "Welcome to Raspberry Pi" dialog.

1. Click "Next"
1. Choose "United States" as Country
1. Choose "American English" as Lanugage
1. Choose "New York" as Timezone
1. Check "Use US Keyboard" and click "Next"
1. Enter the new password and click "Next"
1. Setup a Wi-Fi network if desired
1. We will update software ourselves so you can click "Skip"
1. Click "Reboot"

## Raspbian Configuration

1. Open the "Pi" menu (top left raspberry icon)
1. Choose "Preferences" > "Raspberry Pi Configuration"
1. Click the "Interfaces" tab
1. Enable "SSH" and "VNC"
1. Click "OK"

## Network Setup

1. Connect the Raspberry Pi via Ethernet cable to a switch
1. Ensure your Mac is also connected to the same switch
1. Open a Terminal window (**[>_]** icon in the top toolbar)
1. Type `ifconfig | grep inet` and note the IP address (it will most likely be a 192.168.1.x address, but could be something else)

## Working via SSH
From here on out we will be working on the Raspberry Pi, but we'll be doing it from the confort of our own Mac.

1. Open a Terminal window on your Mac
1. Type `ssh pi@192.168.1.x` or whatever the IP you noted above was
1. You will be prompted the first time you connect with an "Are you sure?", type "yes" and hit Enter
1. Use the password we set above
1. You're in! Feel the power!
