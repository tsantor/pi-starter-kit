#!/usr/bin/env bash

sudo apt update

# Install pygame dependencies
sudo apt install -y \
libavcodec-dev \
libavformat-dev \
libfreetype6-dev \
libjpeg-dev \
libportmidi-dev \
libsdl2-2.0-0 \
libsdl2-image-2.0-0 \
libsdl2-mixer-2.0-0 \
libsdl2-ttf-2.0-0 \
libsmpeg-dev \
libswscale-dev \
python3-dev

# pip install pygame
echo "You may now 'sudo python3 -m pip install pygame' in your virtualenv..."
echo "If you do not use sudo, then you may experience the following error when running pygame.init():"
echo "    NotImplementedError: mixer module not available (ModuleNotFoundError: No module named 'pygame.mixer')"
