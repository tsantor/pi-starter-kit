#!/usr/bin/env bash

# Docker
echo "=> Install Docker"
curl -sSL https://get.docker.com | sh
sudo usermod -aG docker ${USER}

# Docker compose
echo "=> Install Docker Compose"
sudo apt install -y libffi-dev libssl-dev
sudo apt install -y python3-dev
sudo apt install -y python3 python3-pip

sudo python3 -m pip install docker-compose

# Enable the Docker system service to start your containers on boot
echo "=> Enable Docker"
sudo systemctl enable docker
