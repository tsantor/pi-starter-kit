#!/usr/bin/env bash

PYTHON_VERSION=3.12.3

# Update and install some dependencies
echo "=> Install build dependencies"
sudo apt update
sudo apt install -y build-essential checkinstall
sudo apt install -y python3-dev \
libffi-dev \
libssl-dev \
gettext \
libbz2-dev \
libreadline-dev \
libncurses-dev \
sqlite3 \
libsqlite3-dev \
liblzma-dev \
python3-tk

# Download, extract & build
echo "=> Download, extract and build Python"
cd ~
wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
tar xzf Python-${PYTHON_VERSION}.tgz
cd Python-${PYTHON_VERSION}
sudo ./configure --enable-optimizations
sudo make altinstall

# Cleanup
echo "=> Cleanup"
cd ~
sudo rm Python-${PYTHON_VERSION}.tgz
sudo rm -rf Python-${PYTHON_VERSION}

#sudo apt install -y python3-pip

source ~/.profile

python3 -V
