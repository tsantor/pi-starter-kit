#!/usr/bin/env bash

# git clone  https://github.com/pyenv/pyenv.git ~/.pyenv
curl https://pyenv.run | bash

echo 'export PYENV_ROOT="${HOME}/.pyenv"' >> ~/.bash_profile
echo 'export PATH=${PYENV_ROOT}/bin:$PATH' >> ~/.bash_profile
echo 'if which pyenv > /dev/null; then eval "$(pyenv init --path)"; fi' >> ~/.bashrc
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
exec $SHELL -l

# To be safe, this should avoid some future build issues
sudo apt-get install -y \
build-essential \
checkinstall \
curl \
libbz2-dev \
libffi-dev \
libgdbm-dev \
liblzma-dev \
libncurses5-dev \
libncursesw5-dev \
libreadline-dev \
libsqlite3-dev \
libssl-dev \
libxml2-dev \
libxmlsec1-dev \
llvm \
lzma \
lzma-dev \
make \
openssl \
tcl-dev \
tk-dev \
wget \
xz-utils \
zlib1g-dev

sudo apt autoremove -y

echo "\nNow you can run":
echo "    pyenv update"
echo "    pyenv install --list"
echo "    pyenv install 3.9.11"
