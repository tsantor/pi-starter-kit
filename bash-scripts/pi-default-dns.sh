#!/usr/bin/env bash

# Determine which networking service the pi is using
# sudo service --status-all

# Check if NetworkManager service exists
if systemctl list-units --type=service --all | grep -q 'NetworkManager'; then
    echo "NetworkManager service exists."

    # Method 1: Using dhclient
    dns_servers="prepend domain-name-servers 1.1.1.1 1.0.0.1;"
    echo "$dns_servers" | sudo tee -a /etc/dhcp/dhclient.conf
    # sudo service networking restart
    sudo service NetworkManager restart
else
    echo "NetworkManager service does not exist."
fi


# Check if dhcpcd service exists
if systemctl list-units --type=service --all | grep -q 'dhcpcd'; then
    echo "dhcpcd service exists."

    # Method 3: Editing dhcpcd.conf
    dns_servers="static domain_name_servers=1.1.1.1 1.0.0.1"
    echo "$dns_servers" | sudo tee -a /etc/dhcpcd.conf
    sudo systemctl restart dhcpcd
    # sudo service dhcpcd restart
else
    echo "dhcpcd service does not exist."
fi


# Check if dnsutils package is installed
if dpkg -s dnsutils &> /dev/null; then
    echo "=> dnsutils is already installed."
else
    # If not installed, install dnsutils
    echo "=> dnsutils is not installed. Installing..."
    sudo apt update
    sudo apt install -y dnsutils
fi

# Confirm DNS
dig pimylifeup.com
nmcli dev show | grep 'IP4.DNS'
