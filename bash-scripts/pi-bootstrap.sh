#!/usr/bin/env bash

# Enable ssh
echo "=> Enable SSH"
sudo systemctl enable --now ssh

# Update and upgrade packages
sudo apt update && sudo apt -y upgrade

# Install common packages
sudo apt install -y \
bash \
build-essential \
cmake \
coreutils \
curl \
git \
grep \
htop \
nano \
nmap \
openssh-server \
openssl \
rsync \
wget \
whois

# Git
git config --global user.name "Tim Santor"
git config --global user.email "tsantor@xstudios.com"

# Git
# cat << EOF | git config --global --replace-all
# [user]
#     name = Tim Santor
#     email = tsantor@xstudios.com
# EOF
