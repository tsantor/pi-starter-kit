#!/usr/bin/env bash

read -sp 'Permanent Password: ' TV_PASS

sudo apt-get update
sudo apt-get upgrade -y

# Download and install Teamviewer
wget https://download.teamviewer.com/download/linux/teamviewer-host_armhf.deb
sudo dpkg -i teamviewer-host_armhf.deb

# Fix issues with dependencies
sudo apt --fix-broken install -y

# Accept EULA and set password
sudo teamviewer license accept
sudo teamviewer passwd ${TV_PASS}

# Grab the Teamviewer ID
TV_ID=$(sudo teamviewer info | grep "TeamViewer ID:" | grep -o -E '[0-9]{9,}'); echo $TV_ID
echo ""
echo "Login using your ID (${TV_ID}) and password (${TV_PASS})"

echo "You may need to run: sudo teamviewer daemon restart"

rm teamviewer-host_armhf.deb

# To uninstall
# sudo apt remove teamviewer-host
