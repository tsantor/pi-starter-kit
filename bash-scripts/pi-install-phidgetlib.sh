#!/usr/bin/env bash

curl -fsSL https://www.phidgets.com/downloads/setup_linux | sudo -E bash -
sudo apt install -y libphidget22

# Confirm installed
ldconfig -p | grep libphidget22

# ------------------------------------------------------------------------------
# Manual install
# ------------------------------------------------------------------------------
manualInstall() {
    echo "=> Install Phidget Drivers"
    sudo apt install -y libusb-1.0-0-dev
    wget https://cdn.phidgets.com/downloads/phidget22/libraries/linux/libphidget22.tar.gz -P ~
    tar -zxvf ~/libphidget22.tar.gz -C ~

    echo "=> Make install..."
    cd ~/libphidget22-*
    ./configure
    make
    sudo make install

     echo "=> Cleanup..."
    sudo rm ~/libphidget22.tar.gz
    sudo rm -rf ~/libphidget22-*
}

# manualInstall


# If you get error: No Phidgets were detected at all. Make sure your device is attached
# sudo apt install --reinstall udev
