# Pillow dependencies
# https://pillow.readthedocs.io/en/3.0.x/installation.html#external-libraries

sudo apt install -y \
libjpeg-dev \
zlib1g-dev \
libtiff5-dev \
libfreetype6-dev \
liblcms1-dev \
libopenjp2-7-dev
