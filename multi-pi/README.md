This was a WIP, so it's not fully tested.

To do this on multiple targets at once:

- Update `ips.sh` with your Pi ips
- Run `./ssh-copy-id.sh` to get your public key on the devices (no password in the future)
- Run `./copy-tp-pi.sh` to copy all bash scripts to the devices
