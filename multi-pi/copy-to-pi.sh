source ips.sh

for i in "${arr[@]}"; do
    echo "==> $i"
    rsync -avzrp bash-scripts ${i}:/home/pi/

    # Run the script on the machine
    #ssh -t ${i} "./pi-install-node.sh"
done
