source ips.sh

for i in "${arr[@]}"; do
    echo "==> $i"
    ssh-copy-id -i ~/.ssh/id_rsa.pub ${i}
done
